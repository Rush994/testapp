package com.rushapps.addchildapp.data

import android.net.Uri
import io.reactivex.Single
import java.io.File

interface IPhotoRepository {
    fun newFile(): Single<File?>

    fun createPhotoInitializer(file: File): Single<Uri>
}