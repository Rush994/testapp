package com.rushapps.addchildapp.data

import android.content.Context
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import com.rushapps.addchildapp.BuildConfig
import io.reactivex.Single
import java.io.File
import java.util.*
import javax.inject.Inject

class PhotoRepository @Inject constructor(
    private val context: Context): IPhotoRepository {
    override fun newFile(): Single<File?> =
        Single.fromCallable {
            val cal = Calendar.getInstance()
            val timeInMillis = cal.timeInMillis
            val mFileName = "$timeInMillis.jpeg"
            val mFilePath: File? = getFilePath()
            val newFile = File(mFilePath!!.absolutePath, mFileName)
            newFile.createNewFile()
            newFile
        }

    override fun createPhotoInitializer(file: File): Single<Uri> =
        Single.fromCallable {
            val createdPhotoURI = FileProvider.getUriForFile(
                context, "${BuildConfig.APPLICATION_ID}.provider", file
            )
            createdPhotoURI
        }


    private fun getFilePath(): File? =
        context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
}