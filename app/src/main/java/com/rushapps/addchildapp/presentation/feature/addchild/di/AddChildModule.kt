package com.rushapps.addchildapp.presentation.feature.addchild.di

import android.content.Context
import androidx.lifecycle.ViewModel
import com.rushapps.addchildapp.data.IPhotoRepository
import com.rushapps.addchildapp.di.scope.ViewModelKey
import com.rushapps.addchildapp.presentation.feature.addchild.AddChildViewModel
import com.rushapps.addchildapp.presentation.feature.addchild.verifier.IFullFields
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class AddChildModule {
    @Provides
    @IntoMap
    @ViewModelKey(AddChildViewModel::class)
    fun provideViewModel(fullFields: IFullFields, photoRepository: IPhotoRepository, context: Context): ViewModel =
        AddChildViewModel(fullFields = fullFields,
            photoRepository = photoRepository,
            context = context)
}