package com.rushapps.addchildapp.presentation.base

class Constants {
    companion object{
        const val REQUEST_CODE_CAMERA = 200
        const val RESULT_CODE_CAMERA = 201
        const val REQUEST_CODE_GALLERY = 300
        const val RESULT_CODE_GALLERY = 301
        const val APP_TAG = "TestApp"
        const val PERMISSION_TAG = "Permission"

    }
}