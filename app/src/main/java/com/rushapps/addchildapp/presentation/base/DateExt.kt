package com.rushapps.addchildapp.presentation.base

import android.app.DatePickerDialog
import android.widget.TextView
import androidx.fragment.app.Fragment
import java.text.SimpleDateFormat
import java.util.*

fun Fragment.setDate(textView: TextView)
{
    val dateAndTime = Calendar.getInstance()
    val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale("ru","RU"))
    context?.let {
        DatePickerDialog(
            it, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                dateAndTime.set(Calendar.YEAR, year)
                dateAndTime.set(Calendar.MONTH, month)
                dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth)
//                    DateUtils.formatDateTime(
//                    context,
//                    dateAndTime.timeInMillis,
//                    DateUtils.FORMAT_SHOW_DATE or DateUtils.FORMAT_SHOW_YEAR
//                            or DateUtils.FORMAT_SHOW_TIME
//                )
                textView.text = dateFormat.format(dateAndTime.time)
            },
            dateAndTime.get(Calendar.YEAR),
            dateAndTime.get(Calendar.MONTH),
            dateAndTime.get(Calendar.DAY_OF_MONTH)
        )
        .show()
    }
}