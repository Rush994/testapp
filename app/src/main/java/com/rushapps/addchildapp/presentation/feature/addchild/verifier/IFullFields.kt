package com.rushapps.addchildapp.presentation.feature.addchild.verifier

import com.rushapps.addchildapp.presentation.feature.addchild.GenderTypes
import io.reactivex.Single

interface IFullFields {
    var photoValue: String
    var nameValue: String
    var birthValue: String
    var genderType: GenderTypes

    fun verify(): Single<Boolean>
}