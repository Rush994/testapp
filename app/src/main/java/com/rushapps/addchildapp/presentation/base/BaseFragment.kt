package com.rushapps.addchildapp.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import dagger.android.support.DaggerFragment


open class BaseFragment: DaggerFragment {
    constructor(): super()

    var layoutRes = 0
    constructor(layoutRes: Int){
        this.layoutRes = layoutRes
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutRes, container, false)


    fun fragmentNavigate(view: View, destination: Int) =
        Navigation.findNavController(view).navigate(destination, null)

    fun directionNavigate(destination: Int) =
        Navigation.createNavigateOnClickListener(destination)

    fun navBack(view: View, destination: Int) =
        Navigation.findNavController(view).navigate(destination, null)
}