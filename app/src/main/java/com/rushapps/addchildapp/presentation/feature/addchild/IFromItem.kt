package com.rushapps.addchildapp.presentation.feature.addchild

interface IFromItem {
    val permission: String
    val permissionString: String
    val resultCode: Int
}