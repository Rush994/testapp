package com.rushapps.addchildapp.presentation.feature.addchild

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rushapps.addchildapp.R
import com.rushapps.addchildapp.presentation.base.BaseFragment
import com.rushapps.addchildapp.presentation.base.Constants
import com.rushapps.addchildapp.presentation.base.setDate
import com.rushapps.addchildapp.presentation.feature.addchild.verifier.IFullFields
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.dialog_photo_chooser.view.*
import kotlinx.android.synthetic.main.fragment_add_child.*
import javax.inject.Inject


class AddChildFragment : BaseFragment(R.layout.fragment_add_child){

    val logger: (String, String) -> Unit = { str1: String, str2: String -> Log.d(str1, str2) }

    val toaster: (String) -> Unit =
        { str: String ->
            Toast.makeText(context, str, Toast.LENGTH_SHORT).show()
        }

    val withGlide: (String) -> Unit = { str: String ->
        logger(Constants.APP_TAG, str)
        Glide.with(this).load(str)
            .apply(RequestOptions().centerCrop().circleCrop().placeholder(R.drawable.profile_placeholder))
            .into(profilePhoto)
        fullFields.photoValue = str
        viewModel.fieldsVerification()
    }

    val getColor: (Boolean) -> Int = {
        if (!it) R.color.color_red_base
        else R.color.color_disabled_view
    }

    lateinit var rxPermissions: RxPermissions

    @Inject
    lateinit var viewModel: AddChildViewModel

    @Inject
    lateinit var fullFields: IFullFields

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupLiveDataCallbacks(view)

        rxPermissions = RxPermissions(this)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Navigation.findNavController(view).popBackStack()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
        toolbar.setNavigationOnClickListener {
            callback.handleOnBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                Action.FromCamera().resultCode -> viewModel.handleAction(Action.ShowCameraPicture)
                Action.FromGallery().resultCode -> viewModel.handleAction(
                    Action.ShowGalleryPicture(data!!)
                )
            }
        }
    }

    private fun setupView() {
        if (fullFields.photoValue != "")
            withGlide(fullFields.photoValue)
        textInputName.text = Editable.Factory.getInstance().newEditable(fullFields.nameValue)
        textInputBirth.text = Editable.Factory.getInstance().newEditable(fullFields.birthValue)

        with(textInputBirth)
        {
            setOnClickListener { setDate(textInputBirth) }
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    fullFields.birthValue = s.toString()
                    viewModel.fieldsVerification()
                }

                override fun beforeTextChanged(
                    s: CharSequence?, start: Int, count: Int, after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            })
        }
        with(textInputName)
        {
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    fullFields.nameValue = s.toString()
                    viewModel.fieldsVerification()
                }

                override fun beforeTextChanged(
                    s: CharSequence?, start: Int, count: Int, after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            })
        }

        profilePhoto.setOnClickListener { selectImageDialog() }

        buttonSave.setOnClickListener {
            viewModel.verifyOrForward()
        }

        when (fullFields.genderType) {
            GenderTypes.BOY -> {
                chipBoy.isChecked = true
            }
            GenderTypes.GIRL -> {
                chipGirl.isChecked = true
            }
        }
        chipGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.chipBoy -> {
                    fullFields.genderType = GenderTypes.BOY
                }
                R.id.chipGirl -> {
                    fullFields.genderType = GenderTypes.GIRL
                }
            }
        }
    }

    @SuppressLint("InflateParams")
    fun selectImageDialog() {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_photo_chooser, null)
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mDialog = mBuilder.show()

        mDialogView.fromCamera.setOnClickListener {
            mDialog.dismiss()
            rxPermissions
                .request(Action.FromCamera().permission)
                .subscribe{ granted ->
                    if(granted)
                        viewModel.handleAction(Action.FromCamera(permissionString = getString(R.string.all_access_camera)))
                }
        }
        mDialogView.fromGallery.setOnClickListener {
            mDialog.dismiss()
            rxPermissions
                .request(Action.FromGallery().permission)
                .subscribe{ granted ->
                    if(granted)
                        viewModel.handleAction(Action.FromGallery(permissionString = getString(R.string.all_access_gallery)))
                }
        }
        mDialogView.cancel.setOnClickListener {
            mDialog.dismiss()
        }
    }

    private fun setupLiveDataCallbacks(view: View) {
        viewModel.startForResultLiveData.observe(
            viewLifecycleOwner,
            Observer
            { pair ->
                pair?.let { startActivityForResult(it.second, it.first.resultCode) }
            }
        )
        viewModel.setupPhotoLiveData.observe(
            viewLifecycleOwner,
            Observer
            { action -> action?.let { withGlide(it) } }
        )
        viewModel.enableButtonLiveData.observe(
            viewLifecycleOwner,
            Observer
            {
                buttonSave.setBackgroundColor(
                    ContextCompat.getColor(context!!, getColor(it))
                )
            }
        )

        viewModel.goForwardLiveData.observe(
            viewLifecycleOwner,
            Observer
            {
                if (!it) {
                    fragmentNavigate(view, R.id.action_to_newsFragment)
                } else toaster(getString(R.string.all_empty_fields))
            }
        )
    }
}