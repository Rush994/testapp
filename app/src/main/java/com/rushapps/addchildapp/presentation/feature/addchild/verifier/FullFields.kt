package com.rushapps.addchildapp.presentation.feature.addchild.verifier

import com.rushapps.addchildapp.presentation.feature.addchild.GenderTypes
import io.reactivex.Single


class FullFields: IFullFields {
    override var photoValue: String = ""
    override var nameValue: String = ""
    override var birthValue: String = ""
    override var genderType: GenderTypes =
        GenderTypes.GIRL
    override fun verify(): Single<Boolean> {
        var isEmptyFields = false
        if (nameValue == "")
            isEmptyFields = true
        if (birthValue == "")
            isEmptyFields = true
        if (photoValue == "")
            isEmptyFields = true
        return Single.just(isEmptyFields)

    }
}