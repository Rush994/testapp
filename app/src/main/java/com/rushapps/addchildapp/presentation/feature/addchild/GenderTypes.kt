package com.rushapps.addchildapp.presentation.feature.addchild

enum class GenderTypes {
    BOY, GIRL
}