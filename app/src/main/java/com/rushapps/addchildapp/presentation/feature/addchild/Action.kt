package com.rushapps.addchildapp.presentation.feature.addchild

import android.Manifest
import android.content.Intent
import com.rushapps.addchildapp.presentation.base.Constants

sealed class Action {
    object Default : Action()
    data class FromCamera(
        override val permission: String = Manifest.permission.CAMERA,
        override val resultCode: Int = Constants.RESULT_CODE_CAMERA,
        override val permissionString: String = ""
    ) : Action(), IFromItem

    data class FromGallery(
        override val permission: String = Manifest.permission.READ_EXTERNAL_STORAGE,
        override val resultCode: Int = Constants.RESULT_CODE_GALLERY,
        override val permissionString: String = ""
    ) : Action(), IFromItem

    object ShowCameraPicture : Action()
    data class ShowGalleryPicture(val data: Intent) : Action()
}