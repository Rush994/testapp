package com.rushapps.addchildapp.presentation.feature.profile

import android.os.Bundle
import android.view.View
import com.rushapps.addchildapp.R
import com.rushapps.addchildapp.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment: BaseFragment(R.layout.fragment_profile) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(view)
    }

    private fun setupView(view: View)
    {
        buttonAddChild.setOnClickListener(directionNavigate(R.id.action_to_addChildFragment))
    }
}