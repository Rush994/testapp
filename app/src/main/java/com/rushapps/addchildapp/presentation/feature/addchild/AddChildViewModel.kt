package com.rushapps.addchildapp.presentation.feature.addchild

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.MutableLiveData
import com.rushapps.addchildapp.presentation.base.BaseViewModel
import com.rushapps.addchildapp.presentation.base.SingleLiveEvent
import com.rushapps.addchildapp.data.IPhotoRepository
import com.rushapps.addchildapp.presentation.feature.addchild.verifier.IFullFields
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.File
import javax.inject.Inject

class AddChildViewModel @Inject constructor(
    private val fullFields: IFullFields,
    private val photoRepository: IPhotoRepository,
    private val context: Context
) : BaseViewModel() {
    private var createdPhotoURI: Uri
    var startForResultLiveData: SingleLiveEvent<Pair<IFromItem, Intent>>
        private set
    var setupPhotoLiveData: MutableLiveData<String>
        private set
    var enableButtonLiveData: MutableLiveData<Boolean>
        private set
    var goForwardLiveData: SingleLiveEvent<Boolean>
        private set

    init {
        createdPhotoURI = Uri.EMPTY
        enableButtonLiveData = MutableLiveData()
        setupPhotoLiveData = MutableLiveData()
        enableButtonLiveData = MutableLiveData()
        goForwardLiveData = SingleLiveEvent()
        startForResultLiveData = SingleLiveEvent()
    }

    fun handleAction(action: Action) {
        when (action) {
            is Action.FromCamera -> {
                photoRepository.newFile()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { file ->
                        startCamera(file)
                    }
            }
            is Action.FromGallery -> {
                startGallery()
            }
            Action.ShowCameraPicture -> {
                setupPhotoLiveData.apply {
                    value = createdPhotoURI.toString()
                }
            }
            is Action.ShowGalleryPicture -> {
                val selectedImage = action.data.data.toString()
                setupPhotoLiveData.apply {
                    value = selectedImage
                }
            }
        }
    }

    private fun startCamera(file: File?) {
        val photoCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (photoCameraIntent.resolveActivity(context.packageManager) != null) {
            file?.let {
                photoRepository.createPhotoInitializer(file)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { value ->
                        createdPhotoURI = value
                        photoCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, value)
                        startForResultLiveData.value =
                            Pair(
                                first = Action.FromCamera(),
                                second = photoCameraIntent
                            )
                    }.also { disposables.add(it) }
            }
        }
    }

    private fun startGallery() {
        val photoGalleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        ).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startForResultLiveData.apply {
            value = Pair(
                first = Action.FromGallery(),
                second = photoGalleryIntent
            )
        }
    }

    fun fieldsVerification(): Disposable =
        fullFields.verify()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { item ->
                enableButtonLiveData.value = item
            }
            .also { disposables.add(it) }

    fun verifyOrForward(): Disposable =
        fullFields.verify()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { item ->
                goForwardLiveData.value = item
            }

}