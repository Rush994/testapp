package com.rushapps.addchildapp.di.module

import com.rushapps.addchildapp.di.scope.PerActivity
import com.rushapps.addchildapp.presentation.feature.root.EntranceActivity
import com.rushapps.addchildapp.presentation.feature.root.di.EntranceModule
import dagger.Module
import dagger.android.ContributesAndroidInjector
@Module
abstract class ActivityBuildersModule {
    @PerActivity
    @ContributesAndroidInjector(
        modules = [
            EntranceModule::class,
            FragmentBuildersModule::class]
    )
    abstract fun contributeEntranceActivity(): EntranceActivity
}