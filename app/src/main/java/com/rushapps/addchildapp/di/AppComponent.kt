package com.rushapps.addchildapp.di

import android.content.Context
import com.rushapps.addchildapp.TestApp
import com.rushapps.addchildapp.di.module.ActivityBuildersModule
import com.rushapps.addchildapp.di.module.DataModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuildersModule::class,
        DataModule::class]
)
interface AppComponent : AndroidInjector<TestApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }
}