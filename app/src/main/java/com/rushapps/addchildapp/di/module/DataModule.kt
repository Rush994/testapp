package com.rushapps.addchildapp.di.module

import com.rushapps.addchildapp.data.IPhotoRepository
import com.rushapps.addchildapp.data.PhotoRepository
import com.rushapps.addchildapp.presentation.feature.addchild.verifier.FullFields
import com.rushapps.addchildapp.presentation.feature.addchild.verifier.IFullFields
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DataModule {
    @Module
    companion object {
        @Singleton
        @Provides
        @JvmStatic
        fun provideFullFields(): FullFields =
            FullFields()
    }

    @Binds
    abstract fun bindFullFields(source: FullFields): IFullFields

    @Binds
    abstract fun bindPostsRepository(source: PhotoRepository): IPhotoRepository
}