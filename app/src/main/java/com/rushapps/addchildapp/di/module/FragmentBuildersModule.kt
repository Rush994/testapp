package com.rushapps.addchildapp.di.module

import com.rushapps.addchildapp.presentation.feature.addchild.AddChildFragment
import com.rushapps.addchildapp.presentation.feature.addchild.di.AddChildModule
import com.rushapps.addchildapp.presentation.feature.news.NewsFragment
import com.rushapps.addchildapp.presentation.feature.news.di.NewsModule
import com.rushapps.addchildapp.presentation.feature.profile.ProfileFragment
import com.rushapps.addchildapp.presentation.feature.profile.di.ProfileModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector(modules = [AddChildModule::class])
    abstract fun contributeAddChildFragment(): AddChildFragment

    @ContributesAndroidInjector(modules = [ProfileModule::class])
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector(modules = [NewsModule::class])
    abstract fun contributeDataFragment(): NewsFragment
}