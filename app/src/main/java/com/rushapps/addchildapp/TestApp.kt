package com.rushapps.addchildapp

import com.rushapps.addchildapp.di.DaggerAppComponent
import dagger.android.*

class TestApp: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.builder()
            .context(this)
            .build()
}